## Сборка и запуск проекта
Для запуска приложения использовать docker-compose.yml из корня проекта.
Все команды выполнять в корне репизотория.

1. Собрать под проекты. 
> docker-compose build

2. Запустить проект командой: 
> docker-compose up

**После запуска команды docker-compose up необходимо дождаться поднятия инстансов клиента и сервера. Обычно это занимает до одной минуты.**

Видео демонстрации работы - https://drive.google.com/file/d/1DEdFbGtsLfOP6ikJ1Ut4wzcy-_jbT9D2/view?usp=drive_link

После запуска у нас доступны 2 приложения:
Клиент на http://localhost:4173/
Бэкэнд на http://localhost:8000/
Сваггер доступен по адресу http://localhost:8000/docs
Redoc доступен по адресу http://localhost:8000/redoc
