from ultralytics import YOLO

from config import Config


class DetectCarPlateModel:
    def __init__(self):
        # Инициализация модели YOLO с использованием пути к модели, указанного в конфигурации
        self.model = YOLO(Config.YOLO_MODEL_PATH)

    def predict(self, file_path: str):
        # Выполнение предсказания для одного изображения, передавая путь к файлу
        results = self._execute_model(file_paths=[file_path])
        return results[0]

    def predict_series(self, file_paths: list[str]):
        # Выполнение предсказания для серии изображений, передавая список путей к файлам
        results = self._execute_model(file_paths=file_paths)
        return results

    def _execute_model(self, file_paths: list[str]):
        # Выполнение модели YOLO для списка путей к файлам и возвращение результатов
        return self.model(file_paths)
