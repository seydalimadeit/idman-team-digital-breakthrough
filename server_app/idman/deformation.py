import os
from typing import Any

import cv2
import numpy as np

from config import Config
from idman.model import DetectCarPlateModel


class DeformImage:
    def __init__(self):
        # Инициализация модели выравнивания номерных знаков
        self.align_car_plate_model = DetectCarPlateModel()

    def __call__(self, file_path: str, save=False):
        result = self.align_car_plate_model.predict(file_path)
        masks = result.masks

        # Получает координаты маски
        xy = masks.xy
        xy = np.concatenate(xy, axis=0)

        # Определяет угловые точки маски
        top_left = xy[np.argmin(xy[:, 0] + xy[:, 1])]
        bottom_left = xy[np.argmin(xy[:, 0] - xy[:, 1])]
        bottom_right = xy[np.argmax(xy[:, 0] + xy[:, 1])]
        top_right = xy[np.argmax(xy[:, 0] - xy[:, 1])]

        # Определяет цвет границы и отступы
        border_color = [0, 0, 0]
        top = 10
        bottom = top
        left = 10
        right = left

        # Создает массив угловых точек маски
        pts = np.array(
            [top_left, bottom_left, bottom_right, top_right], dtype="float32"
        )

        # Загружает исходное изображение
        orig_image = cv2.imread(file_path)

        # Применяет преобразование четырех точек для выравнивания изображения
        warped = self._four_point_transform(orig_image, pts)
        warped = cv2.resize(warped, (512, 112))
        warped = cv2.copyMakeBorder(
            warped, top, bottom, left, right, cv2.BORDER_CONSTANT, value=border_color
        )

        # Сохраняет результат, если указан параметр save
        if save:
            file_name = os.path.basename(file_path)
            save_path = os.path.join(Config.NORMALIZED_IMAGES_PATH, file_name)
            cv2.imwrite(save_path, warped)
        return warped

    @staticmethod
    def _order_points(pts: np.ndarray[Any, np.dtype]):
        # инициализировать список координат, которые будут упорядочены таким образом,
        # что первая запись в списке находится в верхнем левом углу, вторая запись —
        # в правом верхнем углу, третья — в правом нижнем углу, а четвертая — в левом нижнем углу.
        rect = np.zeros((4, 2), dtype="float32")
        # верхняя левая точка будет иметь наименьшую сумму, тогда как нижняя
        # правая точка будет иметь наибольшую сумму
        s = pts.sum(axis=1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]
        # теперь вычислите разницу между точками: правая верхняя точка будет иметь
        # наименьшую разницу, тогда как нижняя левая будет иметь самую большую разницу.
        diff = np.diff(pts, axis=1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]
        # возвращаем заказанные координаты
        return rect

    def _four_point_transform(self, image, pts: np.ndarray[Any, np.dtype]):
        # получить последовательный порядок точек и распаковать их по отдельности
        rect = self._order_points(pts)
        (tl, tr, br, bl) = rect
        # вычислите ширину нового изображения, которая будет максимальным расстоянием
        # между нижней правой и нижней левой координатами x или верхними правыми и верхними левыми координатами x.
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))
        # compute the height of the new image, which will be the maximum distance between the top-right and
        # bottom-right y-coordinates or the top-left and bottom-left y-coordinates
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))
        # теперь, когда у нас есть размеры нового изображения, создайте набор
        # точек назначения, чтобы получить «вид с высоты птичьего полета» (т.е. вид сверху вниз)
        # изображения, снова указав точки в верхнем левом, верхнем правом, порядок внизу справа и внизу слева
        dst = np.array(
            [
                [0, 0],
                [maxWidth - 1, 0],
                [maxWidth - 1, maxHeight - 1],
                [0, maxHeight - 1],
            ],
            dtype="float32",
        )
        # вычислить матрицу преобразования перспективы и затем применить ее
        M = cv2.getPerspectiveTransform(rect, dst)
        warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
        return warped
