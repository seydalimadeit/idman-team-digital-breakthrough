import os

import cv2
import pandas as pd
from fastapi import FastAPI, UploadFile
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import FileResponse, Response
from starlette.staticfiles import StaticFiles

from config import Config
from idman.deformation import DeformImage
from model import LettersPrediction
from template import apply_template

app = FastAPI()

origins = [
    "*",
]
app.mount("/normalized", StaticFiles(directory="normalized_images", html=True), name="normalized")
app.mount("/default_images", StaticFiles(directory="images", html=True), name="images")

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

regions_type = [2, 3]
model = LettersPrediction()
deform_image = DeformImage()


@app.post("/images/normalize")
async def create_upload_file(files: list[UploadFile]) -> FileResponse:
    result = []
    for file in files:
        file_name = Config.DATASET_PATH / file.filename
        result.append(
            {
                "image_name": file.filename,
                "prediction_region_length_2": "",
                "prediction_region_length_3": "",
            }
        )
        with open(file_name, "wb") as f:
            f.write(file.file.read())
        img = deform_image(file_path=file_name, save=True)
        img = cv2.resize(img, (512, 112))
        for region_type in regions_type:
            crops = apply_template(img, region_type)
            lp_number = model.predict_series(crops)
            result[-1][f"prediction_region_length_{region_type}"] = lp_number
    pd.DataFrame(result).to_csv(Config.CSV_FILE_NAME, index=False)

    return FileResponse(Config.CSV_FILE_NAME)


@app.get("/images/{file_name}/normalized")
async def create_upload_file(file_name: str):
    for root, dirs, files in os.walk(Config.NORMALIZED_IMAGES_PATH):
        if file_name in files:
            return FileResponse(f"{Config.NORMALIZED_IMAGES_PATH}/{file_name}")
    return Response(status_code=404)

