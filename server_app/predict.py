from pathlib import Path

import cv2
import pandas as pd
from tqdm import tqdm

from config import Config
from idman.deformation import DeformImage
from model import LettersPrediction
from template import apply_template

data_path = Path(Config.DATASET_PATH)

regions_type = [2, 3]
model = LettersPrediction()
deform_image = DeformImage()

result = []
for p in tqdm(data_path.iterdir()):

    if not p.suffix in [".png", ".jpg", ".jpeg"]:
        continue
    result.append(
        {
            "image_name": p.stem,
            "prediction_region_length_2": "",
            "prediction_region_length_3": "",
        }
    )

    # img = cv2.imread(str(p))

    # вызов фунции деформации
    img = deform_image(file_path=str(p), save=True)

    img = cv2.resize(img, (512, 112))

    for region_type in regions_type:
        crops = apply_template(img, region_type)

        lp_number = model.predict_series(crops)
        result[-1][f"prediction_region_length_{region_type}"] = lp_number

pd.DataFrame(result).to_csv(Config.CSV_FILE_NAME, index=False)


# Normalize plate length in columns
df = pd.read_csv(Config.CSV_FILE_NAME)


def modify_column(value, length):
    value = str(value)
    if len(value) > length:
        return value[:length]
    else:
        return value.ljust(length, '0')


# Apply the function to the appropriate columns
df['prediction_region_length_2'] = df['prediction_region_length_2'].apply(lambda x: modify_column(x, 8))
df['prediction_region_length_3'] = df['prediction_region_length_3'].apply(lambda x: modify_column(x, 9))

# Save the modified DataFrame back to CSV
df.to_csv(Config.CSV_FILE_NAME, index=False)
