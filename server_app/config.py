from pathlib import Path


class Config:
    BASE_DIR = Path(__file__).resolve().parent

    DATASET_PATH = BASE_DIR / "images"

    MODEL_PATH = BASE_DIR / "resnet18_letters.pth"
    YOLO_MODEL_PATH = BASE_DIR / "idman/idman_yoloV8-seg.pt"

    NORMALIZED_IMAGES_PATH = BASE_DIR / 'normalized_images'

    CSV_FILE_NAME = BASE_DIR / "sample_submission.csv"
